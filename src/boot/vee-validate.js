// import something here
  import {
    extend,
    ValidationObserver,
    ValidationProvider,
    setInteractionMode
  } from 'vee-validate'
  import {
    required,
    email,
    max,
    min,
    confirmed
  } from 'vee-validate/dist/rules'


// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ({ Vue }) => {
  /**
   *  Create components and set vee-validation settings
   */
  setInteractionMode('eager');

  extend('required', {
    ...required,
    message: '{_field_} can not be empty',
  })

  extend('max', {
    ...max,
    message: '{_field_} may not be greater than {length} characters',
  })

  extend('confirmed', {
    ...confirmed,
    message: '{_field_} must be confirmed',
  })

  extend('min', {
    ...min,
    message: '{_field_} may not be less than {length} characters',
  })

  extend('email', {
    ...email,
    message: 'This must be a valid Email',
  })

  Vue.component('ValidationProvider', ValidationProvider);
  Vue.component('ValidationObserver', ValidationObserver);
}
