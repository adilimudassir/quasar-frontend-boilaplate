// import something here

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ({
  Vue, store
}) => {
  Vue.mixin({
    methods: {
      userCan(permission) {
        if (store.getters['auth/permissions'] === [] || store.getters['auth/permissions'].length == 0) return false
        return this.$store.getters['auth/permissions'].includes(permission)
      },
      userCanAny(permissions) {
        if (store.getters['auth/permissions'] === [] || store.getters['auth/permissions'].length == 0) return false
        return permissions.some(permission => store.getters['auth/permissions'].includes(permission))
      }
    }

  });
}
