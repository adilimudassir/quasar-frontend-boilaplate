import Vue from 'vue';
import VueRouter from 'vue-router';
import {
  LoadingBar
} from 'quasar'
import {
  sync
} from 'vuex-router-sync'
import routes from './routes';


Vue.use(VueRouter);

LoadingBar.setDefaults({
  color: 'blue',
  size: '15px',
  position: 'bottom'
})

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

let Store = null

export default function ({
  store,
  ssrContext
}) {
  Store = store;
  
  const router = new VueRouter({
    scrollBehavior,
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
  });

  sync(store, router)

  router.beforeEach(async (to, from, next) => {
  
    LoadingBar.start();
    
    if (! store.getters['auth/user'] && store.getters['auth/token']) {
      try {
        await store.dispatch('auth/fetchUser');
      } catch (e) {
        await store.dispatch('auth/logout')
      }
    }

    next();
  })

  router.afterEach((to, from) => {
    LoadingBar.stop();
  })

  return router;
}

function scrollBehavior(to, from, savedPosition) {
  if (savedPosition) {
    return savedPosition;
  }

  const position = {};

  if (to.hash) {
    position.selector = to.hash;
  }

  if (to.matched.some(m => m.meta.scrollToTop)) {
    position.x = 0;
    position.y = 0;
  }

  return position;
}

/**
 * Redirect to login if guest.
 *
 * @param  {Array} routes
 * @return {Array}
 */
export function authGuard(routes) {
  return beforeEnter(routes, (to, from, next) => {
    if (!Store.getters['auth/check']) {
      next({
        name: 'login',
        query: {
          redirect: to.fullPath
        }
      })
    } else {
      next()
    }
  })
}

/**
 * Redirect home if authenticated.
 *
 * @param  {Array} routes
 * @return {Array}
 */
export function guestGuard(routes) {
  return beforeEnter(routes, (to, from, next) => {
    if (Store.getters['auth/check']) {
      next({
        name: 'dashboard',
      })
    } else {
      next()
    }
  })
}

/**
 * Apply beforeEnter guard to the routes.
 *
 * @param  {Array} routes
 * @param  {Function} beforeEnter
 * @return {Array}
 */
function beforeEnter(routes, beforeEnter) {
  return routes.map(route => {
    return {
      ...route,
      beforeEnter
    }
  })
}
