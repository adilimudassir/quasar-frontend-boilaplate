import {
  guestGuard,
  authGuard
} from './index'

const routes = [
...authGuard([{
  path: '/',
  component: () => import('layouts/Auth.vue'),
  alias: '/api/email/verify/:id/:hash',
  children: [{
    path: '',
    name: 'dashboard',
    component: () => import('pages/Index.vue'),
  },
  {
    path: '/profile',
    name: 'user.profile',
    component: () => import('pages/Profile')
  },
  {
    path: '/users',
    name: 'users.index',
    component: () => import('pages/authentication/users/Index'),
  },
  {
    path: '/users/:id/',
    name: 'users.show',
    component: () => import('pages/authentication/users/Show'),
    children: [{
      path: 'overview',
      name: 'users.overview',
      component: () => import('pages/authentication/users/Overview'),
    },
    {
      path: 'password',
      name: 'users.password',
      component: () => import('pages/authentication/users/Password'),
    },
    ]
  },
  {
    path: '/roles',
    name: 'roles.index',
    component: () => import('pages/authentication/roles/Index')
  },
  {
    path: '/categories',
    name: 'categories.index',
    component: () => import('pages/categories/Index')
  }, {
    path: '/categories/create',
    name: 'categories.create',
    component: () => import('pages/categories/Form')
  },
  {
    path: '/categories/edit/:id',
    name: 'categories.edit',
    component: () => import('pages/categories/Form')
  },
  {
    path: '/items',
    name: 'items.index',
    component: () => import('pages/items/Index')
  },
  {
    path: '/items/create',
    name: 'items.create',
    component: () => import('pages/items/Form')
  },
  {
    path: '/items/edit/:id',
    name: 'items.edit',
    component: () => import('pages/items/Form')
  },
  {
    path: '/customers',
    name: 'customers.index',
    component: () => import('pages/customers/Index')
  },
  {
    path: '/customers/create',
    name: 'customers.create',
    component: () => import('pages/customers/Form')
  },
  {
    path: '/customers/edit/:id',
    name: 'customers.edit',
    component: () => import('pages/customers/Form')
  }
  ],
}]),

...guestGuard([{
  path: '/',
  component: () => import('layouts/Guest.vue'),
  children: [{
    path: 'login',
    name: 'login',
    component: () => import('pages/Login.vue'),
  }, {
    path: '/password/email',
    name: 'forgot-password',
    component: () => import('pages/ForgotPassword.vue'),
  },
  {
    path: '/password/reset',
    name: 'forgot-password',
    component: () => import('pages/ResetPassword.vue'),
  }
  ],
}]),

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue'),
  },
  ];

  export default routes;
