import Base from './Base';

class Role extends Base {
    constructor() {
        super();
    }

    all() {
        return this.api.get('/permissions');
    }
}

export default new Role();
