import Base from './Base';

class Auth extends Base {
    constructor() {
        super();
    }

    login(form) {
        return this.api.post('/login', form);
    }

    register(form) {
        return this.api.post('/register', form);
    }

    verifyMail(id, hash, query) {
      return this.api.get(`/email/verify/${id}/${hash}`, null, query);
    }

    resendVerificationMail() {
      return this.api.post('/email/resend');
    }

    requestPasswordResetLinkEmail(form) {
      return this.api.post('/password/email', form);
    }

    resetPassword(form) {
      return this.api.post('/password/reset', form);
    }

    logout() {
        return this.api.post('/logout');
    }

    profile() {
        return this.api.get('/user');
    }
}

export default new Auth();
