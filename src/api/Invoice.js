import Base from './Base';
import {
  map,
  pick
} from 'lodash';

class Invoice extends Base {
  constructor() {
    super();
  }

  all(filter = null) {
    return this.api.get('/invoices', filter);
  }

  create(form) {
    return this.api.post('/invoices/create', form);
  }

  find(id) {
    return this.api.get(`/invoices/${id}/find`);
  }

  update(id, form) {
    return this.api.patch(`/invoices/${id}/update`, form);
  }

  delete(id) {
    return this.api.delete(`/invoices/${id}/delete`);
  }
  async forSelectOptions() {
    const {
      data
    } = await this.all();

    return map(data, (invoice) => pick(invoice, ['id', 'name']))
  }
}

export default new Invoice();
