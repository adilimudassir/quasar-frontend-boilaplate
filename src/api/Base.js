import Api from '../plugins/axios';

export default class Base {
    constructor() {
        this.api = Api;
    }

    test() {
        return this.api.get('/');
    }
}
