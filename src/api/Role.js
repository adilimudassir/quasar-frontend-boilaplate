import Base from './Base';
import {
  map,
  pick
} from 'lodash';

class Role extends Base {
  constructor() {
    super();
  }

  all(filter = null) {
    return this.api.get('/roles', filter);
  }

  create(form) {
    return this.api.post('/roles/create', form);
  }

  find(id) {
    return this.api.post(`/roles/${id}/find`);
  }

  update(id, form) {
    return this.api.patch(`/roles/${id}/update`, form);
  }

  delete(id) {
    return this.api.delete(`/roles/${id}/delete`);
  }

  async forSelectOptions() {
    const {
      data
    } = await this.all();

    return map(data, (role) => pick(role, ['id', 'name']))
  }
}

export default new Role();
