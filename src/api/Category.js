import Base from './Base';

class Category extends Base {
  constructor() {
    super();
  }

  all(filter = null) {
    return this.api.get('/categories', filter);
  }

  create(form) {
    return this.api.post('/categories/create', form);
  }

  find(id) {
    return this.api.get(`/categories/${id}/find`);
  }

  update(id, form) {
    return this.api.patch(`/categories/${id}/update`, form);
  }

  delete(id) {
    return this.api.delete(`/categories/${id}/delete`);
  }
}

export default new Category();
