import Base from './Base';
import {
  map,
  pick
} from 'lodash';

class Customer extends Base {
  constructor() {
    super();
  }

  all(filter = null) {
    return this.api.get('/customers', filter);
  }

  create(form) {
    return this.api.post('/customers/create', form);
  }

  find(id) {
    return this.api.get(`/customers/${id}/find`);
  }

  update(id, form) {
    return this.api.patch(`/customers/${id}/update`, form);
  }

  delete(id) {
    return this.api.delete(`/customers/${id}/delete`);
  }

  async forSelectOptions() {
    const {
      data
    } = await this.all();

    return map(data, (customer) => pick(customer, ['id', 'name']))
  }
}

export default new Customer();
