import Base from './Base';

class User extends Base {
  constructor() {
    super();
  }

  all(filter = null) {
    return this.api.get('/users', filter);
  }

  create(form) {
    return this.api.post('/users/create', form);
  }

  find(id) {
    return this.api.get(`/users/${id}/find`);
  }

  update(id, form) {
    return this.api.patch(`/users/${id}/update`, form);
  }

  delete(id) {
    return this.api.delete(`/users/${id}/delete`);
  }

  updatePassword(id, form) {
    return this.api.patch(`/users/${id}/update-password`, form);
  }
}

export default new User();
