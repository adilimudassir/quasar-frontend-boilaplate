import Base from './Base';
import {
  map,
  pick
} from 'lodash';

class Item extends Base {
  constructor() {
    super();
  }

  all(filter = null) {
    return this.api.get('/items', filter);
  }

  create(form) {
    return this.api.post('/items/create', form);
  }

  find(id) {
    return this.api.get(`/items/${id}/find`);
  }

  update(id, form) {
    return this.api.patch(`/items/${id}/update`, form);
  }

  delete(id) {
    return this.api.delete(`/items/${id}/delete`);
  }
  async forSelectOptions() {
    const {
      data
    } = await this.all();

    return map(data, (item) => pick(item, ['id', 'name']))
  }
}

export default new Item();
