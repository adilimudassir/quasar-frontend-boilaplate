import axios from 'axios';
import {
  Cookies,
  Notify
} from 'quasar'

const Api = axios.create({
  baseURL: `${process.env.API_URL}/api/`,
  withCredentials: false
});

Api.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Api.interceptors.request.use(request => {
  if (Cookies.get('token')) {
    request.headers.common['Authorization'] = `Bearer ${Cookies.get('token')}`
  }
  return request
})

Api.interceptors.response.use(response => {
  switch (response.status) {
    case 250:
      Notify.create({
        type: "positive",
        message: "Email Verified Successfully!",
        progress: true,
        position: "top"
      })
      break;
    case 251:
      Notify.create({
        type: "info",
        message: "Email already verified!",
        progress: true,
        position: "top"
      })
      break;
    // case 252:
    //   Notify.create({
    //     type: "warning",
    //     message: "Your Email is currently not verified!",
    //     progress: true,
    //     position: "top"
    //   })
    //   break;
    case 253:
      Notify.create({
        type: "warning",
        message: "Invalid Email Verification Code!",
        progress: true,
        position: "top"
      })
      break;
    // case 254:
    //   Notify.create({
    //     type: "info",
    //     message: "Email Verification link sent! Please Check your inbox!",
    //     progress: true,
    //     position: "top"
    //   })
    //   break;
  }
  return response;
}, (error) => {
  if (error.response) {
    switch (error.response.status) {
      case 422:
        Notify.create({
          type: "negative",
          message: error.response.data.message,
          progress: true,
          position: "top"
        })
        break;
      case 401:
        Notify.create({
          type: "warning",
          message: "You are not authorized to perform this request!",
          progress: true,
          position: "top"
        })
        break;
    }
  } else if (error.request) {
    Notify.create({
      type: "negative",
      message: "Apologies! we could not proceed with your request right now. Please try again in a bit!",
      progress: true,
      position: "top"
    })
  } else {
    Notify.create({
      type: "negative",
      message: "Apologies! Your request could not be processed. Please check and try again!",
      progress: true,
      position: "top"
    })
  }
  return error;
})

export default Api;
