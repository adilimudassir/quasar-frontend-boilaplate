import * as types from './mutation-types'

export default {
  [types.TOGGLE_SIDEBAR](state) {
    state.isSidebarOpen = !state.isSidebarOpen
  },

  [types.TABLE_LOADING](state, isLoading) {
    state.tableLoading = isLoading;
  },
}
