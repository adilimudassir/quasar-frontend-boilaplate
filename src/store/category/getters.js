import {
  map,
  pick
} from 'lodash';

export const categories = (state) => state.categories

export const getCategoryById = (state) => (id) => {
  return state.categories.find(category => category.id === id)
}

export const categoriesForSelectOptions = (state) => {
  return map(state.categories, (category) => pick(category, ['id', 'name']))
}