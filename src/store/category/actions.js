import * as types from './mutation-types';
import * as generalTypes from '../mutation-types';
import Category from '../../api/Category';

export const fetchCategories = ({ commit, dispatch, state }, params) => {
  commit(generalTypes.TABLE_LOADING, true, {
    root: true
  })
  return new Promise((resolve, reject) => {
    Category.all({ params })
      .then((response) => {
        commit(types.SET_CATEGORIES, response.data)
        commit(generalTypes.TABLE_LOADING, false, { root: true })
        resolve(response)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export const findCategory = ({ commit, dispatch, state }, id) => {
  return new Promise((resolve, reject) => {
      Category.find(id)
      .then((response) => {
        resolve(response)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export const addCategory = ({ commit, dispatch, state }, data) => {
  return new Promise((resolve, reject) => {
    Category.create(data)
      .then((response) => {
        commit(types.ADD_CATEGORY, response.data)
        resolve(response)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export const updateCategory = ({ commit, dispatch, state }, data) => {
  return new Promise((resolve, reject) => {
    Category.update(data.id, data.form)
      .then((response) => {
        commit(types.UPDATE_CATEGORY, response.data)
        resolve(response)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export const deleteCategory = ({ commit, dispatch, state }, id) => {
  return new Promise((resolve, reject) => {
    Category.delete(id)
      .then((response) => {
        if (response.status === 200) {
          commit(types.DELETE_CATEGORY, id)
        }
        resolve(response)
      })
      .catch((err) => {
        reject(err)
      })
  })
}
