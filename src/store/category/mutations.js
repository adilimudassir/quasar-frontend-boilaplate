import * as types from './mutation-types'

export default {
  [types.SET_CATEGORIES](state, categories) {
    state.categories = categories
  },

  [types.ADD_CATEGORY](state, category) {
    state.categories.push(category)
  },

  [types.UPDATE_CATEGORY](state, data) {
    let pos = state.categories.findIndex(
      (category) => category.id === data.id
    )
    Object.assign(state.categories[pos], { ...data })
  },

  [types.DELETE_CATEGORY](state, id) {
    let pos = state.categories.findIndex((category) => category.id === id)
    state.categories.splice(pos, 1)
  },
}
