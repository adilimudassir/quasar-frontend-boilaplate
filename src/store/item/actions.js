import * as types from './mutation-types';
import * as generalTypes from '../mutation-types';
import Item from '../../api/Item';

export const fetchItems = ({ commit, dispatch, state }, params) => {
  commit(generalTypes.TABLE_LOADING, true, {
    root: true
  })
  return new Promise((resolve, reject) => {
    Item.all({ params })
      .then((response) => {
        commit(types.SET_ITEMS, response.data)
        commit(generalTypes.TABLE_LOADING, false, { root: true })
        resolve(response)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export const findItem = ({ commit, dispatch, state }, id) => {
  return new Promise((resolve, reject) => {
      Item.find(id)
      .then((response) => {
        resolve(response)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export const addItem = ({ commit, dispatch, state }, data) => {
  return new Promise((resolve, reject) => {
    Item.create(data)
      .then((response) => {
        commit(types.ADD_ITEM, response.data)
        resolve(response)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export const updateItem = ({ commit, dispatch, state }, data) => {
  return new Promise((resolve, reject) => {
    Item.update(data.id, data.form)
      .then((response) => {
        commit(types.UPDATE_ITEM, response.data)
        resolve(response)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export const deleteItem = ({ commit, dispatch, state }, id) => {
  return new Promise((resolve, reject) => {
    Item.delete(id)
      .then((response) => {
        if (response.status === 200) {
          commit(types.DELETE_ITEM, id)
        }
        resolve(response)
      })
      .catch((err) => {
        reject(err)
      })
  })
}
