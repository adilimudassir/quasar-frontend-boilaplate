import * as types from './mutation-types'

export default {
  [types.SET_ITEMS](state, items) {
    state.items = items
  },

  [types.ADD_ITEM](state, item) {
    state.items.push(item)
  },

  [types.UPDATE_ITEM](state, data) {
    let pos = state.items.findIndex(
      (item) => item.id === data.id
    )
    Object.assign(state.items[pos], { ...data })
  },

  [types.DELETE_ITEM](state, id) {
    let pos = state.items.findIndex((item) => item.id === id)
    state.items.splice(pos, 1)
  },
}
