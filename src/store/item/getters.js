export const items = (state) => state.items

export const getItemById = (state) => (id) => {
  return state.items.find(item => item.id === id)
}