import  { Cookies } from 'quasar';

export default function () {
    return {
        user: null,
        permissions: [],
        isVerified: null,
        token: Cookies.get('token') ?? null,
    };
}
