import Auth from '../../api/Auth';

export function saveToken({ commit }, payload) {
    commit('SAVE_TOKEN', payload);
}

export function fetchUser({ commit }) {
        Auth.profile()
            .then(res => {
                commit('FETCH_USER_SUCCESS', {
                    auth: res.data,
                });
            }).catch(err => {
                commit('FETCH_USER_FAILURE');
            });
}

export async function updateUser({ commit }, payload) {
    commit('UPDATE_USER', payload);
}

export async function logout({ commit }) {
    commit('LOGOUT');
}
