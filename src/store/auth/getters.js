export function user(state) {
  return state.user;
}

export function permissions(state) {
  return state.permissions;
}

export function token(state) {
  return state.token;
}

export function check(state) {
  return state.token !== null;
}

export function isVerified(state) {
  return state.isVerified;
}
