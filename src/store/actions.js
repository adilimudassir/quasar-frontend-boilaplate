import * as types from './mutation-types'

export default {
  toggleSidebar({ commit }) {
    commit(types.TOGGLE_SIDEBAR)
  },
  loadTable({ commit }, isLoading) {
    commit(types.TABLE_LOADING, isLoading)
  },
}
