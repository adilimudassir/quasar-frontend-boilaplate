import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth';
import category from './category';
import item from './item';
import customer from './customer';
import state from './state';
import * as getters from './getters'
import mutations from './mutations'
import actions from './actions'

import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

 export default function ( /* { ssrContext } */ ) {

  const Store = new Vuex.Store({
    state,
    getters,
    mutations,
    actions,
    modules: {
      auth,
      category,
      item,
      customer,
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING,
    plugins: [createPersistedState({
      storage: window.sessionStorage
    })]
  });

  return Store;
}
