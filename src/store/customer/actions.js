import * as types from './mutation-types';
import * as generalTypes from '../mutation-types';
import Customer from '../../api/Customer';

export const fetchCustomers = ({ commit, dispatch, state }, params) => {
  commit(generalTypes.TABLE_LOADING, true, {
    root: true
  })
  return new Promise((resolve, reject) => {
    Customer.all({ params })
    .then((response) => {
      commit(types.SET_CUSTOMERS, response.data)
      commit(generalTypes.TABLE_LOADING, false, { root: true })
      resolve(response)
    })
    .catch((err) => {
      reject(err)
    })
  })
}

export const findCustomer = ({ commit, dispatch, state }, id) => {
  return new Promise((resolve, reject) => {
    Customer.find(id)
    .then((response) => {
      resolve(response)
    })
    .catch((err) => {
      reject(err)
    })
  })
}

export const addCustomer = ({ commit, dispatch, state }, data) => {
  return new Promise((resolve, reject) => {
    Customer.create(data)
    .then((response) => {
      commit(types.ADD_CUSTOMER, response.data)
      resolve(response)
    })
    .catch((err) => {
      reject(err)
    })
  })
}

export const updateCustomer = ({ commit, dispatch, state }, data) => {
  return new Promise((resolve, reject) => {
    Customer.update(data.id, data.form)
    .then((response) => {
      commit(types.UPDATE_CUSTOMER, response.data)
      resolve(response)
    })
    .catch((err) => {
      reject(err)
    })
  })
}

export const deleteCustomer = ({ commit, dispatch, state }, id) => {
  return new Promise((resolve, reject) => {
    Customer.delete(id)
    .then((response) => {
      if (response.status === 200) {
        commit(types.DELETE_CUSTOMER, id)
      }
      resolve(response)
    })
    .catch((err) => {
      reject(err)
    })
  })
}
