import * as types from './mutation-types'

export default {
  [types.SET_CUSTOMERS](state, customers) {
    state.customers = customers
  },

  [types.ADD_CUSTOMER](state, customer) {
    state.customers.push(customer)
  },

  [types.UPDATE_CUSTOMER](state, data) {
    let pos = state.customers.findIndex(
    (item) => customer.id === data.id
    )
    Object.assign(state.customers[pos], { ...data })
  },

  [types.DELETE_CUSTOMER](state, id) {
    let pos = state.customers.findIndex((customer) => customer.id === id)
    state.customers.splice(pos, 1)
  },
}
